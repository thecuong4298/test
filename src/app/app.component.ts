import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'demoo';

  ngOnInit() {
    async () => await this.waitme();
    console.log('hehe');
  }

  waitme() {
    new Promise(resolve => {
      setTimeout( () => {
        console.log('hus');
          resolve(1);
      }, 1000);
    })
  }
}
